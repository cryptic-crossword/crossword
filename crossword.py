BLANK,BLACK ='W','B'
size=15
crossword=[]

    
for i in range(0,size):
        m=input()
        x=list(m)
        crossword.append(x)
 

def is_crossword_column(crossword: list[list[str]], row: int, col: int) -> bool:
    if crossword[row][col] == BLANK:
        if row == 0 or (crossword[row - 1][col] == BLACK and row != 0) and row + 1 < size:
            if crossword[row + 1][col] == BLANK :
                return True
    return False
	


def rows_check(matrix: list[list[str]],row,column: int) -> bool:
    
    if matrix[row][column] == BLANK:
        
        if column == 0 and matrix[row][column + 1] == BLANK:
            return True
        elif column != 0 and matrix[row][column - 1] == BLACK and column +1<size:
             if matrix[row][column +1] == BLANK:
                  return True
         
        else:
            return False
    
    else:
        return False
    
numbering = 0 
rows=0
col=0

def result(crossword : list[list[str]]) -> list[tuple]:
    global numbering  
    result_list = []
    for rows in range (0,size):
        for col in range (0,size):
            
            if is_crossword_column(crossword , rows, col) or rows_check(crossword, rows, col):
                numbering += 1
                result_list.append((rows, col, numbering))
    
    return result_list


print(result(crossword))


